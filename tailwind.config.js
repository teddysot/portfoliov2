module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    extend: {
      margin: {
        '72': '18rem',
        '80': '20rem',
      },
      screens: {
        'mobile': '375px',
      },
      width: {
        'screen-2': '200vw',
        'screen-3': '300vw',
        'screen-4': '400vw',
        'screen-40' : '40vw',
        'screen-60' : '60vw',
        'screen-70' : '70vw',
        'screen-75' : '75vw',
        'screen-80' : '80vw',
      },
      height: {
        '72': '18rem',
        '80': '20rem',
        '88': '22rem',
        '96': '24rem',
        '104': '26rem',
        'screen-0': '0vh',
        'screen-10': '10vh',
        'screen-20': '20vh',
        'screen-30': '30vh',
        'screen-80': '80vh',
      },
      listStyleType: {
        none: 'none',
        disc: 'disc',
        decimal: 'decimal',
        square: 'square',
        roman: 'upper-roman',

      }
    },
  },
  variants: {},
  plugins: [],
}
