const typeeffect = document.getElementById('typeeffect');

const typewriter = new Typewriter(typeeffect, {
    autoStart: true,
    loop: true
});

typewriter.typeString(`Hello <span class="primary-text-color">Visitor</span>!`)
    .pauseFor(2500)
    .deleteAll()
    .typeString(`I'm <span class="primary-text-color">Saharat Nasahachart</span>`)
    .pauseFor(2500)
    .deleteChars(19)
    .typeString('<span class="primary-text-color">a Software Engineer</span>')
    .pauseFor(2500)
    .start();

const scrollOnClick =() => {
    $(document).ready(function () {
        // Add smooth scrolling to all links
        $("a").on('click', function (event) {

            // Make sure this.hash has a value before overriding default behavior
            if (this.hash !== "") {
                // Prevent default anchor click behavior
                event.preventDefault();

                // Store hash
                var hash = this.hash;

                // Using jQuery's animate() method to add smooth page scroll
                // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
                $('html, body').animate({
                    scrollTop: $(hash).offset().top
                }, 500, function () {

                    // Add hash (#) to URL when done scrolling (default click behavior)
                    window.location.hash = hash;
                });
            } // End if
        });
    });
}

